from setuptools import find_packages, setup

setup(
    name="urltestlib",
    packages=find_packages(include=["urltestlib"]),
    version="0.1.0",
    description="Generate reports to test url reachability",
    author="Me",
    license="MIT",
    install_requires=[],
    setup_requires=["pytest-runner"],
    tests_require=["pytest==4.4.1"],
    test_suite="tests",
)
