# URLTESTLIB

urltestlib is a Python library used to test the reachability of urls, and generate reports.

## Installation
The library is not yet available via PyPi...
In that sense, you will have to pull the library from git, and then build the library by running in your command prompt:
```bash
python setup.py bdist_wheel
```

Your wheel file is stored in the "dist" folder that has just been created. You can finally install the library using:
```bash
pip install /path/to/wheelfile.whl
```

## Usage
```python
from urltestlib.report import Report
url_list = [
    "https://www.nfinite.app/",
    "https://towardsdatascience.com/",
    "http://www.google.com/nothere",
]
rep = Report(url_list)
print(rep.check_urls())
```

The library's fonctionnalities have also been wrapped inside an API, and is available on this repository:
https://gitlab.com/p.grignon33/urltestlib-api

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Licence
[MIT](https://choosealicense.com/licenses/mit/)
