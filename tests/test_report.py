from urltestlib import report


def test_report():
    url_list = [
        "https://www.nfinite.app/",
        "https://towardsdatascience.com/",
        "http://www.google.com/nothere",
    ]
    result = {
        "report": [
            {
                "url": "https://www.nfinite.app/",
                "available": True,
                "status_code": 200,
                "reason": "OK",
            },
            {
                "url": "https://towardsdatascience.com/",
                "available": True,
                "status_code": 200,
                "reason": "OK",
            },
            {
                "url": "http://www.google.com/nothere",
                "available": False,
                "status_code": 404,
                "reason": "Not Found",
            },
        ]
    }
    rep = report.Report(url_list)
    assert rep.check_urls() == result
