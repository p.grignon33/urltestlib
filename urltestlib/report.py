import requests
from typing import List, Dict
from datetime import datetime
import json
import logging
import boto3

# Config logger
logging.basicConfig(
    level=logging.INFO,
    filename="app.log",
    filemode="a",
    format="%(asctime)s - %(levelname)s - %(message)s",
)


class Report:
    """
    The report object contains a lot of urls to test !

    Attributes:
        urls (List[str]): all the urls to be tested for reachability
    """

    def __init__(self, urls: List) -> None:
        self.urls = urls

    def check_urls(self) -> Dict:
        """
        Check if the urls are reachable

        Returns:
            report: The report for the urls and their reachability
        """
        report = []
        for url in self.urls:
            try:
                request = requests.get(url)
                url_report = {
                    "url": url,
                    "available": request.ok,
                    "status_code": request.status_code,
                    "reason": request.reason,
                }
                logging.info(
                    f"Succesfully tested url {url} resulting in a {request.status_code} status code"
                )
            except requests.exceptions.RequestException as e:
                logging.error(f"Exception {e} for url {url}")

            report.append(url_report)
        return {"report": report}

    def check_urls_to_json(self) -> str:
        """
        Check if the urls are reachable and save the report in json format

        Returns:
            filename: the name of the json file
        """
        report = self.check_urls()
        timestamp = int(datetime.timestamp(datetime.now()))
        filename = f"report_{timestamp}.json"
        with open(filename, "w") as fp:
            json.dump(report, fp)
            logging.info(f"Stored the report in file {filename}")
        return filename

    def check_urls_to_bucket_s3(self, s3conn: boto3.resource, bucket_name: str) -> str:
        """
        Check if the urls are reachable and save the report in S3 bucket

        Parameters:
            s3conn: connection to the S3 bucket
            bucket_name: the name of the s3 bucket for storage

        Returns:
            filename: the name of the json file
        """
        # Create bucket if it does not exist
        if bucket_name not in [bucket.name for bucket in s3conn.buckets.all()]:
            s3conn.create_bucket(Bucket=bucket_name, CreateBucketConfiguration={'LocationConstraint': 'eu-west-3'})

        report = self.check_urls()
        timestamp = int(datetime.timestamp(datetime.now()))
        filename = f"report_{timestamp}.json"

        s3object = s3conn.Object(bucket_name, filename)

        s3object.put(
            Body=(bytes(json.dumps(report).encode('UTF-8')))
        )
        return filename